var indexPageController = (function(ProductService, ProductSearchParameters){

    //use this for filtering via ProductService
    var searchParameters = new ProductSearchParameters({
        sortBy: 'price',
        direction: 'asc'
    });
    //target item for product list
    var container = document.querySelector('.result .product-items');
    //category items list container
    var categories = document.querySelectorAll('.filters ul li');
    //search form
    var searchForm = document.querySelector('.index-page nav .search form');
    //search input
    var searchInput = document.querySelector('.index-page nav .search form input');

    function initialize(){
        searchProduct(searchParameters);

        searchForm.addEventListener('submit', (event) => {
            event.preventDefault();
            searchProduct(searchParameters);
        });

        searchInput.addEventListener('input', (event) => {
            searchParameters.searchTerm = event.target.value;
        });

        for ( let i=0; i < categories.length; i++ ) {         
            categories[i].addEventListener('click', () => {
                searchParameters.category = event.target.innerText;
                searchProduct(searchParameters);
            });
        }      
    }

    function searchProduct(searchParameters) {
        
        //@todo: implement searching
        ProductService.search(searchParameters)
            .then( function(products) {
                container.innerHTML = '';
                products.forEach(function(product) {
                    container.appendChild(product.display());
                });
            });

            if ( searchParameters.category != null ) {
                for ( let i=0; i < categories.length; i++ ) {
                    if ( categories[i].innerText == searchParameters.category ) 
                        categories[i].setAttribute('class' , 'active');
                    else categories[i].removeAttribute('class');
                }
                searchParameters.category = null;
            } else {
                for ( let i=0; i < categories.length; i++ ) {
                    if ( categories[i].hasAttribute('class') )
                     categories[i].removeAttribute('class');
                }
            }

            if ( searchParameters.searchTerm != '') searchParameters.searchTerm = ''; 
    }

    return {
        run: initialize
    };
})(ProductService, ProductSearchParameters);