/**
 * EPAM training 2020
 * homework-03-#2
 * Asteriks Patterns 
 * writed by gabor szekeres
 */

function pattern (numberOfLevels, position) {

    if (position == 'left') {
        let asteriks = "";
        for (let index = 0; index < numberOfLevels; index++) {
            asteriks += "*";
            console.log(asteriks); 
        } 

    } else if (position == 'center') {

        for (let i = numberOfLevels; i > 0; i--) {
            let asteriks = "";
            for ( let j = 1; j < numberOfLevels*2 ; j++ ) {

                asteriks += j < i  || j > ((numberOfLevels*2) - i) ? " " : "*";
            }
            console.log(asteriks);
        }
        
    } else if ( position == 'right' ) {

        for (let i = 0; i < numberOfLevels; i++) {
            let asteriks = "";
            for ( let j = 1; j <= numberOfLevels; j++ ) {

                asteriks += j < numberOfLevels-i ? " " : "*";
            }
            console.log(asteriks);
        }
    }
}
pattern(4, 'left');