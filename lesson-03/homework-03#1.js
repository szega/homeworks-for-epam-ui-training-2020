/**
 * EPAM training 2020
 * homework-03-#1
 * Array of People Objects
 * writed by gabor szekeres
 */

var attrs = 'name age city gender';
var values = [
    ['Joe', 22, 'New York City', 'male'],
    ['Jane', 85, 'Las Vegas', 'female'],
    ['Jack', 55, 'London', 'male']
];

var properties = attrs.split(' ');
var people = [];

for (let i = 0; i < values.length; i++) {
    let aPeople = {};
    for (let j = 0; j < properties.length; j++) {
        aPeople[properties[j]] = values[i][j];
    }
    people.push(aPeople);
}
console.log("People's array: ".toUpperCase() + "\n");
console.log(people);
