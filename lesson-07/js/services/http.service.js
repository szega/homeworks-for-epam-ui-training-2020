class Http {

    fetch;

    constructor() {
        this.fetch = window.fetch;
    }

    get(url, headers) {

        var config = {
            method: 'GET',
            headers: headers || {}
        };

        console.log('GET', url);

        return fetch(url, config)
            .then( (response) => {
                if (!response.ok) {
                    return Promise.reject({
                        status: response.status,
                        message: response.statusText
                    });
                }
                return response.json();
            })
            .then( (response) => {
                return this._delay(response);
            });
    }

    _delay(response){
        return new Promise( (resolve) => {
            setTimeout(resolve.bind(null, response), parseInt(Math.random() * 2000 + 500));
        });
    }

    getQueryString(params){
        return Object.keys(params)
            .filter( (key) => { return !!params[key] })
            .map( (key) => { return `${key}=${params[key]}` }).join('&');
    }

    notImplemented(url, headers){
        return Promise.reject('Not implemented');
    }
}

