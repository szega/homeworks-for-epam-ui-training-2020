class CommentService {
    http;
    params;

    constructor(){
        this.http = new Http();
    }

    fetchComments(postId, top, skip) {

        const params = {
            postId: postId,
            _limit: top || 4,
            _start: skip || 0
        };

        return this.http.get(`https://jsonplaceholder.typicode.com/comments?${this.http.getQueryString(params)}`);
    }
}