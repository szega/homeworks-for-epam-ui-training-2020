class PostService {
    http;

    constructor() {
        this.http = new Http()
    }
    
    fetchPosts(userId, top, skip) {
        const params = {
            userId: userId,
            _limit: top || 3,
            _start: skip || 0
        };

        return this.http.get(`https://jsonplaceholder.typicode.com/posts?${this.http.getQueryString(params)}`);
    }
}