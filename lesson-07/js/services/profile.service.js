
class ProfileService {
    http;
    constructor(){
        this.http = new Http();
    }
    fetchProfile(){
        return this.http.get('https://jsonplaceholder.typicode.com/users/1').then( (profile) => {
            return this._createProfileFromResponse(profile);
        });
    }
    _createProfileFromResponse(response){
        return {
            id: response.id,
            name: response.name,
            email: response.email
        };
    }

}