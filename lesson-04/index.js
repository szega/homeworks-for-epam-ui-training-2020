(function() {

    // a simple Circle object"
    var aSimpleCircle = new Circle(4);

    // random numbers for the first call minimum.run()
    var numbers = [];
    for ( let index = 0; index < 10; index++ ) {
        numbers[index] = Math.round(Math.random()*100);
    }
    var minValue = minimum.run(...numbers);

    // an object for Circle
    // var aCircle = new Circle(4);
    var aCircle = CircleModule.run(12);
    
    // an object for the first calls of Laptop.run()
    var aLaptop = Laptop.run('Lenovo', 1024);

    // an object for the first calls of LaptopProto.run();
    var aLaptopProto = LaptopProto.run('Asus', 512);
    
    logger.log(`A simple circle object is, from new Circle(4) -->  area: ${aSimpleCircle.area}, circumference: ${aSimpleCircle.circumference}`);
    logger.log(`\nThe init runs of modules are,\n\n-- minimum.run(${numbers}) --> ${minValue}`);
    logger.log(`\n-- Laptop.run('Lenovo', 1024) -->`);
    logger.log(aLaptop);
    logger.log(`\n-- LaptopProto.run('Asus', 512) and LaptopProto.prototype.turnOn() -->`);
    logger.log(aLaptopProto);
    logger.log(`\n-- Circle "class" as module. CircleModule.run(12) --> area: ${aCircle.area}, circumference: ${aCircle.circumference}`)
})();
