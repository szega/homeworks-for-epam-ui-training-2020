var Laptop = ( function() {

    var Laptop = function(brand, emptySpace) {
        this.brand = brand;
        this.emptySpace = emptySpace;
        this.occupiedSpace = 0;
        this.isTurnedOn = false;
        this.isOpened = false;
    }
    
    function run() {
        return new Laptop(arguments[0], arguments[1]);
    }

    return { 
        run: run 
    };

})();