var minimum = ( function() {

    var min = function() {
        var m = arguments[0];
        for (var i = 1; i < arguments.length; i++) {
            if (arguments[i] < m) {
                m = arguments[i];
            }
        } 
        return m;
    }

    function run() {
        return min(...arguments);
    }

    return { 
        run: run 
    };

})();