var LaptopProto = ( function() {

    var LaptopProto = function(brand, emptySpace) {
        this.brand = brand;
        this.emptySpace = emptySpace;
        this.occupiedSpace = 0;
    }

    function run() {
        LaptopProto.prototype.isTurnedOn = false;
        LaptopProto.prototype.isOpened = false;
        
        LaptopProto.prototype.turnOn = function() {
            this.isTurnedOn = true;
            this.isOpened = true;
        }
        var aLaptop = new LaptopProto(arguments[0], arguments[1]);
        aLaptop.turnOn();
        return aLaptop;
    }

    return { run: run };
})();