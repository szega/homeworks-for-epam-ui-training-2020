
var CircleModule = ( function() {

    var CircleModule = function(r) {
        this.radius = r;
    }
    
    Object.defineProperty(CircleModule.prototype, 'area', {
        get: function () { 
            return this.radius * this.radius * Math.PI; 
        } 
    });
    
    Object.defineProperty(CircleModule.prototype, 'circumference', {
        get: function () {
            return 2 * this.radius * Math.PI; 
        }
    });
    
    function run() { 
        return new CircleModule(arguments[0]);
    }

    return {
        run: run
    };
    
})();